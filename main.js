const img = document.querySelectorAll('.category-label');
const menu = document.querySelector('.nav-group');
const menuBtn = document.querySelector('.menu-open');
const topBar = document.querySelector('.top-bar');

let click = 0;
menuBtn.addEventListener('click',()=>{
    click ? click-- : click++
    click ? menu.classList.add('opened') : menu.classList.remove('opened')
    menuBtn.classList.toggle('active');
});

img.forEach(item =>{
    const color = item.getAttribute('data-labelColor');
    item.style.backgroundColor = color;
});



